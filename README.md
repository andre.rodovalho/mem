# mem

Kinda like "free" command but output in percent.

I'd like to have output from `free` proportional to the total available memory but there's no `"free -p"` so. It's not good code but it's helpful.

# Dependencies

This depends on `bc` and you may need to install it on your system

# Usage

Move the `mem` script to a directory on your PATH (maybe `~/.local/bin/` or `/usr/bin/`) and make it executable.

```
$ mem
	 total 		 used 		 free 		 available
Mem: 	 100% 		 40% 		 13% 		 50%
Swap: 	 100% 		 0% 		 100%
```
